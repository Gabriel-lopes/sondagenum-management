package controleur;

public class TableauBord {
	private String nom, email, tel, adresse, role;

	public TableauBord(String nom, String email, String tel, String adresse, String role) {
		super();
		this.nom = nom;
		this.email = email;
		this.tel = tel;
		this.adresse = adresse;
		this.role = role;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	} 
	
	
	
}