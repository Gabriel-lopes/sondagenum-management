package controleur;

import java.sql.Date;

public class Sondage {
	private int idSondage, idSondeur, idSous_categorie;
	private String dateCreation;
	private String libelle;
	private String etat; //'Ouvert' ou 'Ferme'
	private String dateFin;
	
	public Sondage(int idSondage, String libelle, String dateCreation, String etat,String dateFin , int idSondeur, int idSous_categorie) {
		
		this.idSondage = idSondage;
		this.idSondeur = idSondeur;
		this.idSous_categorie = idSous_categorie;
		this.dateCreation = dateCreation;
		this.libelle = libelle;
		this.etat = etat;
		this.dateFin = dateFin;
	}
	
	public Sondage(String libelle, String dateCreation,String etat,String dateFin, int idSondeur, int idSous_categorie) {
		
		this.idSondage = 0;
		this.idSondeur = idSondeur;
		this.idSous_categorie = idSous_categorie;
		this.dateCreation = dateCreation;
		this.libelle = libelle;
		this.etat = etat;
		this.dateFin = dateFin;
	}

	
	public int getIdSondage() {
		return idSondage;
	}

	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}

	public int getIdSondeur() {
		return idSondeur;
	}

	public void setIdSondeur(int idSondeur) {
		this.idSondeur = idSondeur;
	}

	public int getIdSous_categorie() {
		return idSous_categorie;
	}

	public void setIdSous_categorie(int idSous_categorie) {
		this.idSous_categorie = idSous_categorie;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}
	
	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	
}
