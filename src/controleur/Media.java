package controleur;

public class Media {
	private int idMedia, idSondage, idSondeur;
	private String url;
	
	public Media(int idMedia, int idSondage, int idSondeur, String url) {

		this.idMedia = idMedia;
		this.idSondage = idSondage;
		this.idSondeur = idSondeur;
		this.url = url;
	}
	
	public Media(int idSondage, int idSondeur, String url) {

		this.idMedia = 0;
		this.idSondage = idSondage;
		this.idSondeur = idSondeur;
		this.url = url;
	}

	public int getIdMedia() {
		return idMedia;
	}

	public void setIdMedia(int idMedia) {
		this.idMedia = idMedia;
	}

	public int getIdSondage() {
		return idSondage;
	}

	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}

	public int getIdSondeur() {
		return idSondeur;
	}

	public void setIdSondeur(int idSondeur) {
		this.idSondeur = idSondeur;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
