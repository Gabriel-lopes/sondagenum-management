package controleur;

public class Reponse {
	private int idReponse, nbReponse,idQuestion;
	private String reponse;
	
	public Reponse(int idReponse, int nbReponse, int idQuestion, String reponse) {
		this.idReponse = idReponse;
		this.nbReponse = nbReponse;
		this.idQuestion = idQuestion;
		this.reponse = reponse;
	}
	
	public Reponse(int nbReponse, int idQuestion, String reponse) {
		this.idReponse = 0;
		this.nbReponse = nbReponse;
		this.idQuestion = idQuestion;
		this.reponse = reponse;
	}

	public int getIdReponse() {
		return idReponse;
	}

	public void setIdReponse(int idReponse) {
		this.idReponse = idReponse;
	}

	public int getNbReponse() {
		return nbReponse;
	}

	public void setNbReponse(int nbReponse) {
		this.nbReponse = nbReponse;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	
}
