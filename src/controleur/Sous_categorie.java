package controleur;

public class Sous_categorie {
	private int idSous_categorie, idCategorie;
	private String nom, description;
	
	public Sous_categorie(int idSous_categorie , String nom, String description, int idCategorie ) {
		this.idSous_categorie = idSous_categorie;
		this.idCategorie = idCategorie;
		this.nom = nom;
		this.description = description;
	}
	
	public Sous_categorie(String nom, String description, int idCategorie) {
		this.idSous_categorie = 0;
		this.nom = nom;
		this.description = description;
		this.idCategorie = idCategorie;
	}

	public int getIdSous_categorie() {
		return idSous_categorie;
	}

	public void setIdSous_categorie(int idSous_categorie) {
		this.idSous_categorie = idSous_categorie;
	}

	public int getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(int idCategorie) {
		this.idCategorie = idCategorie;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
