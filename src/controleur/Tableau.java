package controleur;

import javax.swing.table.AbstractTableModel;

public class Tableau extends AbstractTableModel{

	private String entetes [];
	private Object donnees [][];
	
	public Tableau (Object donnees [][], String entetes []) {
		this.donnees = donnees;
		this.entetes = entetes;
	}
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return this.donnees.length;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return this.entetes.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return this.donnees[rowIndex][columnIndex];
	}

	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return this.entetes[column];
	}
	
	//Ajouter une ligne au tableau pour le rendre dynamique
	public void ajouterLigne(Object ligne[]) {
		Object matrice [][] = new Object [this.donnees.length+1][this.entetes.length];
		for(int i = 0; i < this.donnees.length; i++) {
			matrice[i] = donnees[i];
		}
		matrice[this.donnees.length] = ligne;
		
		//On ecrase l'ancienne matrice avec la nouvelle
		this.donnees = matrice;
		//On actualise l'affichage
		this.fireTableDataChanged();
	}

	public void supprimerLigne(int numLigne) {
		Object matrice [][] = new Object [this.donnees.length-1][this.entetes.length];
		int j = 0;
		for(int i = 0; i < this.donnees.length; i++) {
			if(i != numLigne) {
				matrice[j] = this.donnees[i];
				j++;
			}
		}
		//On ecrase apres la donnee avec la nouvelle matrice
		this.donnees = matrice;
		//On actualise
		this.fireTableDataChanged();
	}

	public void modifierLigne(int numLigne, Object[] ligne) {
		Object matrice [][] = new Object [this.donnees.length][this.entetes.length];
		for(int i = 0; i < this.donnees.length; i++) {
			if(i != numLigne) {
				matrice[i] = this.donnees[i];
			} else {
				matrice[i] = ligne;
			}
		}
		//On ecrase apres la donnee avec la nouvelle matrice
		this.donnees = matrice;
		//On actualise
		this.fireTableDataChanged();
	}

	public void setDonnees(Object[][] matrice) {
		//On ecrase apres la donnnee avec la nouvelle matrice
		this.donnees = matrice;
		//On actualise l'affichage
		this.fireTableStructureChanged();
	}
}
