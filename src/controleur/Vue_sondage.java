package controleur;

import java.sql.Date;

public class Vue_sondage {
	private int idSondage, idSous_categorie, idQuestion;
	private String libelle, question, nom, etat;
	private Date dateCreation;
	
	public Vue_sondage(int idSondage, int idSous_categorie, int idQuestion, String libelle, String question, String nom,
			String etat, Date dateCreation) {
		
		this.idSondage = idSondage;
		this.idSous_categorie = idSous_categorie;
		this.idQuestion = idQuestion;
		this.libelle = libelle;
		this.question = question;
		this.nom = nom;
		this.etat = etat;
		this.dateCreation = dateCreation;
	}

	public int getIdSondage() {
		return idSondage;
	}

	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}

	public int getIdSous_categorie() {
		return idSous_categorie;
	}

	public void setIdSous_categorie(int idSous_categorie) {
		this.idSous_categorie = idSous_categorie;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	
}
