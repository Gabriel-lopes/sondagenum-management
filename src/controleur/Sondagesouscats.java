package controleur;

import java.sql.Date;

public class Sondagesouscats {
	private int idSondage, idSous_categorie;
	private String libelle, etat, nom;
	private Date dateCreation;
	
	public Sondagesouscats(int idSondage, int idSous_categorie, String libelle, String etat, String nom,
			Date dateCreation) {
	
		this.idSondage = idSondage;
		this.idSous_categorie = idSous_categorie;
		this.libelle = libelle;
		this.etat = etat;
		this.nom = nom;
		this.dateCreation = dateCreation;
	}
	
	public Sondagesouscats(int idSous_categorie, String libelle, String etat, String nom,
			Date dateCreation) {
	
		this.idSondage = 0;
		this.idSous_categorie = idSous_categorie;
		this.libelle = libelle;
		this.etat = etat;
		this.nom = nom;
		this.dateCreation = dateCreation;
	}

	public int getIdSondage() {
		return idSondage;
	}

	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}

	public int getIdSous_categorie() {
		return idSous_categorie;
	}

	public void setIdSous_categorie(int idSous_categorie) {
		this.idSous_categorie = idSous_categorie;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	
	
}
	

