package modele;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Bdd 
{
	private String server, bdd, user, mdp;
	private Connection maConnexion;
	
	
	public Bdd(String server, String bdd, String user, String mdp) {
		this.server = server;
		this.bdd = bdd;
		this.user = user;
		this.mdp = mdp;
		this.maConnexion = null;
	}
	
	public void seConnecter()
	{
		//Methode permet de se connecter � la base de donn�e.
		String url = "jdbc:mysql://"+this.server+"/"+this.bdd+"?verifyServerCertificate=false&useSSL=false";; 
		url += "?verifyServerCertificate=false&useSSL=false";
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
		}
		catch(ClassNotFoundException exp)
		{
			System.out.println("Absence du pilote JDBC");
		}
		
		try
		{
			this.maConnexion = DriverManager.getConnection(url, this.user, this.mdp);
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur de connexion � :" + url);
		}
		
	}
	
	public void seDeConnecter()
	{
		//Methode permet de se d�connecter de la base de donn�e.
		try
		{
			if(this.maConnexion!= null)
			{
				this.maConnexion.close();
			}
		}
		catch(SQLException exp)
		{
			System.out.println("Erreur de fermeture de la connexion");
		}
	}

	public Connection getMaConnexion() {
		return maConnexion;
	}

	public void setMaConnexion(Connection maConnexion) {
		this.maConnexion = maConnexion;
	}
}
