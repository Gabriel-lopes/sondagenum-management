package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controleur.Sondeur;
import controleur.Tableau;
import controleur.TableauBord;
import modele.Modele;

public class PanelBord extends PanelDeBase implements ActionListener{
	
	private JTable uneTable;
	private Tableau unTableau;
	private JScrollPane uneScroll;
	
	private JTextField txtMot = new JTextField();
	private JButton btRechercher = new JButton("Rechercher");
	private JPanel panelRechercher = new JPanel();
	
	public PanelBord() {
		super(new Color(0x0F417A));
		
		//Construction du panel de recherche
		this.panelRechercher.setBounds(120, 25, 500, 30);
		this.panelRechercher.setLayout(new GridLayout(1,2)); // 3 Lignes / 1 colone
		this.panelRechercher.add(new JLabel ("Rechercher un sondeur : "));
		this.panelRechercher.add(this.txtMot);
		this.panelRechercher.add(this.btRechercher);
		this.add(this.panelRechercher);
		
		//Construction du panel tableau de bord
		String entetes [] = {"Nom", "email", "tel", "adresse", "role"};
		this.unTableau = new Tableau(this.getTableauBord(""), entetes);
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(120, 60, 500, 350);
		this.add(this.uneScroll);
		
		this.btRechercher.addActionListener(this);
	}
	
	public Object [][] getTableauBord(String mot) {
		//methode qui transforme l'ArrayList des pilotes en une matrice [][]
		ArrayList<TableauBord> lesTableauxBords = Modele.selectAllTableauxBords(mot);
		//le tableau de tableaux contient les pilotes suivis de leurs informations
		//Les pilotes = lesPilotes (arraylist) //// 5 = nombre de champs d'un pilote en comptant son id
		Object matrice [][] = new Object[lesTableauxBords.size()][5];
		int i = 0;
		for(TableauBord unTab : lesTableauxBords) {
			matrice[i][0] = unTab.getNom();
			matrice[i][1] = unTab.getEmail();
			matrice[i][2] = unTab.getTel();
			matrice[i][3] = unTab.getAdresse();
			matrice[i][4] = unTab.getRole();

			i++;
		}
		return matrice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == this.btRechercher) {
			String mot = this.txtMot.getText();
			this.unTableau.setDonnees(this.getTableauBord(mot));
		}
	}
	
	
}
