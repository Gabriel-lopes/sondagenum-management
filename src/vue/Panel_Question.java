package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controleur.Categorie;
import controleur.Question;
import controleur.Sondage;
import controleur.Sondeur;
import controleur.Sous_categorie;
import controleur.Tableau;

import modele.Modele;

public class Panel_Question extends PanelDeBase implements ActionListener {
	
	private JTable uneTable;
	private JScrollPane uneScroll;
	private Tableau unTableau;
	
	private JTextField txtMot = new JTextField();
	private JButton btRechercher = new JButton("Rechercher");
	private JPanel panelRechercher = new JPanel();
	
	
	
	public Panel_Question()
	{
		super(new Color(0x0F417A));
		
		//Construction du panel Rechercher
		this.panelRechercher.setBounds(50, 40, 640, 30);
		this.panelRechercher.setLayout(new GridLayout(1, 3));
		this.panelRechercher.add(new JLabel("Filtrer les questions par : "));
		this.panelRechercher.add(this.txtMot);
		this.panelRechercher.add(this.btRechercher);
		this.add(this.panelRechercher);
		
		
		//Construction de la JScroll afin de lister les pilotes
		String entetes [] = {"idQuestion ", "question", "idSondage"};
		this.unTableau = new Tableau(getQuestions(""), entetes);
		//Creer la table 
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(10, 100, 700, 180);
		this.add(this.uneScroll);
		
		
		//	Traitement de la suppression 
		this.uneTable.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				int numLigne = uneTable.getSelectedRow();
				if(e.getClickCount() == 2) 
				{
					int idQuestion = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
					int retour = JOptionPane.showConfirmDialog(null, "Voulez-Vous supprimer la question", "Suppression de la question", JOptionPane.YES_NO_OPTION);
					if(retour == 0)
					{
						Modele.deleteQuestion(idQuestion);
						unTableau.supprimerLigne(numLigne);
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//Rendre les boutons ecoutable
		this.btRechercher.addActionListener(this);
		
	}
	
	public Object [][] getQuestions(String mot)
	{
		//Methode qui transforme l'ArrayList des pilotes en une matrice [] []
		ArrayList<Question> lesQuestions = Modele.selectAllQuestion(mot);
		//Je cree ma matrice
		Object matrice [][] = new Object[lesQuestions.size()][3];
		int i = 0;
		for(Question uneQuestion : lesQuestions)
		{
			matrice[i][0] = uneQuestion.getIdQuestion();
			matrice[i][1] = uneQuestion.getQuestion();
			matrice[i][2] = uneQuestion.getIdSondage();
			i++;
		}
		
		return matrice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.btRechercher)
		{
			String mot = this.txtMot.getText();
			this.unTableau.setDonnees(this.getQuestions(mot));
		}
	}
}
