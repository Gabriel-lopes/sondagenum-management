package vue;


import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controleur.SondageNum;
import controleur.Sondeur;

import javax.swing.JPasswordField;

public class VueConnexion extends JFrame implements ActionListener, KeyListener {
	
	private JPanel unPanel = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btSeConnecter = new JButton("Se connecter");
	private JTextField txtEmail = new JTextField();
	private JPasswordField txtMdp= new JPasswordField();
	
	public VueConnexion() {
		this.setTitle("Connexion au Logiciel Admin Sondage-Num");
		this.setBounds(200, 100, 700, 300);
		this.getContentPane().setBackground(new Color(0x0F417A));
		this.setResizable(false);
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Construire le panel :
		this.unPanel.setBounds(350, 50, 300, 180);
		this.unPanel.setLayout(new GridLayout(3,2));
		this.unPanel.setBackground (new Color(0x0F417A));
		
		JLabel unEmail = new JLabel("Email : "); 
		this.unPanel.add(unEmail);
		unEmail.setForeground(Color.white);
		this.unPanel.add(txtEmail);
		
		
		JLabel unMdp = new JLabel("MDP : ");
		this.unPanel.add(unMdp);
		unMdp.setForeground(Color.white);
		this.unPanel.add(txtMdp);
		this.unPanel.add(this.btAnnuler);
		this.unPanel.add(this.btSeConnecter);
		
		//Ajout d'une image
		ImageIcon uneImage = new ImageIcon("src/images/Logo.png");
		JLabel logo = new JLabel(uneImage);
		logo.setBounds(10, 15, 330, 180);
		this.add(logo);
		
		this.add(this.unPanel); //Ajout du panel dans la fen�tre
		
		Font unePolice = new Font("", Font.ITALIC, 16);
		
		
		//Rendre les boutons �coutable :
		this.btAnnuler.addActionListener(this);
		this.btSeConnecter.addActionListener(this);
		this.txtEmail.addKeyListener(this);
		this.txtMdp.addKeyListener(this);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		if(e.getSource() == this.btAnnuler) 
		{ //Si on annule le formulaire
			this.txtEmail.setText("");
			this.txtMdp.setText("");
		}else if(e.getSource() == this.btSeConnecter)
		{ 
			traitement();
		}
	}	
	
	public void traitement()
	{
		//Si on se connecte
		String email = this.txtEmail.getText();
		String mdp = new String (this.txtMdp.getPassword());
		
		Sondeur unUser = SondageNum.selectWhereUser(email, mdp);
		if(unUser == null) {
			JOptionPane.showMessageDialog(this,  "Veuillez vérifier vos identifiants");
			this.txtEmail.setText("");
			this.txtMdp.setText("");
		//Si l'utilisateur n'est pas admin
		}else if(!unUser.getRole().equals("admin")) {
			JOptionPane.showMessageDialog(this, "Vous n'avez pas la permission de vous connectez !");
		}else {
			JOptionPane.showMessageDialog(this,  "Bienvenue "
					+ unUser.getNom()
					+"\n\n Vous avez le role de : " + unUser.getRole());
			
			//Lancer l'application vueGenerale
			SondageNum.instancierVueGenerale(unUser);
			
			//On rend invisible la vue connexion
			SondageNum.rendreVisibleVueConnexion(false);
			this.txtMdp.setText("");
			}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ENTER)
		{
			traitement();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		
	}
}