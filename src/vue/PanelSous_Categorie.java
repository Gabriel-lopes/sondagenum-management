package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controleur.Sous_categorie;
import controleur.Tableau;
import modele.Modele;

public class PanelSous_Categorie extends PanelDeBase implements ActionListener{
	
	private JPanel panelForm = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btEnregistrer = new JButton("Enregistrer");
	private JTextField txtNom = new JTextField();
	private JTextField txtDescription = new JTextField();
	private JComboBox<String> txtidCategorie = new JComboBox<String>();

	
	private JTable uneTable;
	private JScrollPane uneScroll;
	private Tableau unTableau;
	
	
	private JTextField txtMot = new JTextField();
	private JButton btRechercher = new JButton("Rechercher");
	private JPanel panelRechercher = new JPanel();
	
	public PanelSous_Categorie() {
		super(new Color(0x0F417A)); //Appel du paneldebase avec la couleur gris

		//Construction du panelForm
		this.panelForm.setBounds(10, 40, 250, 240);
		this.panelForm.setLayout(new GridLayout(5,2)); // 5 Lignes / 2 colones
		this.panelForm.add(new JLabel ("Nom de la Sous-catégorie : "));
		this.panelForm.add(this.txtNom);
		this.panelForm.add(new JLabel ("Description de la Sous-catégorie : "));
		this.panelForm.add(this.txtDescription);
		this.panelForm.add(new JLabel("ID de la Categorie: "));
		this.panelForm.add(this.txtidCategorie);
		this.panelForm.add(btAnnuler);
		this.panelForm.add(btEnregistrer);
		this.add(panelForm);
		
		//Construction du panel rechercher
		this.panelRechercher.setBounds(290, 40, 400, 30);
		this.panelRechercher.setLayout(new GridLayout(1,3)); // 3 Lignes / 1 colone
		this.panelRechercher.add(new JLabel ("Filtrer les Sous-catégories par : "));
		this.panelRechercher.add(this.txtMot);
		this.panelRechercher.add(this.btRechercher);
		this.add(this.panelRechercher);
		
		this.panelForm.setVisible(true);
		
		//Construction de la JScroll lister les Sous-Categories
		String entetes [] = {"idSous_categorie","idCategorie", "nom", "description"};
		this.unTableau = new Tableau(getCategories(""), entetes);
		
		
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(290, 100, 400, 180);
		this.add(this.uneScroll);
		
		//Traitement de la suppression
		this.uneTable.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int numLigne = uneTable.getSelectedRow();
				if(e.getClickCount() == 2) {
					int idSous_categorie = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
					int retour = JOptionPane.showConfirmDialog(null, "Voulez-vous supprimer la Sous-catégorie ?", "Suppression de Sous-Categorie", JOptionPane.YES_NO_OPTION);
					if(retour == 0) {
						Modele.deleteSous_categorie(idSous_categorie); //deletePilote
						unTableau.supprimerLigne(numLigne);
					}
				} 
				else if (e.getClickCount() == 1) {
					txtNom.setText(unTableau.getValueAt(numLigne, 2).toString());
					txtDescription.setText(unTableau.getValueAt(numLigne, 3).toString());
					btEnregistrer.setText("Modifier"); //On transforme le bouton enregistrer en modifier
				}
			}
		});
		
		//Bouton cliquable
		this.btAnnuler.addActionListener(this);
		this.btEnregistrer.addActionListener(this);
		this.btRechercher.addActionListener(this);
		
		//Appele la methode pour le panel
		this.remplirCombo();
		
	}
	
	public Object [][] getCategories(String mot) {
		//methode qui transforme l'ArrayList des pilotes en une matrice [][]
		ArrayList<Sous_categorie> lesSous_categories = Modele.selectAllSousCategories(mot);
		//le tableau de tableaux contient les pilotes suivis de leurs informations
		//Les pilotes = lesPilotes (arraylist) //// 5 = nombre de champs d'un pilote en comptant son id
		Object matrice [][] = new Object[lesSous_categories.size()][4];
		int i = 0;
		for(Sous_categorie uneSous_categorie : lesSous_categories) {
			matrice[i][0] = uneSous_categorie.getIdSous_categorie();
			matrice[i][1] = uneSous_categorie.getIdCategorie();
			matrice[i][2] = uneSous_categorie.getNom();
			matrice[i][3] = uneSous_categorie.getDescription();
			i++;
		}
		return matrice;
	}
	
	//Remplir les Combo
		public void remplirCombo() {
				ArrayList<Sous_categorie> lesSous_categories = Modele.selectAllSousCategories();
				for(Sous_categorie uneSous_categorie : lesSous_categories)
				{
					this.txtidCategorie.addItem(uneSous_categorie.getIdCategorie()+"-"+uneSous_categorie.getNom());
				}
		}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == this.btAnnuler) {
			this.viderChamps();
		}
		else if(e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Enregistrer")) {
			this.traitement(1);
		}
		else if(e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Modifier")) {
			this.traitement(2);
		}
		else if(e.getSource() == this.btRechercher) {
			String mot = this.txtMot.getText();
			this.unTableau.setDonnees(this.getCategories(mot));
		}
	}
	
	//Fonction qui vide les champs du formulaire si "Annuler"
	public void viderChamps() {
		this.txtNom.setText("");
		this.txtDescription.setText("");
	}
	
	//Fonction qui traite l'enregistement
	public void traitement(int choix) {
		String chaine = this.txtidCategorie.getSelectedItem().toString();
		String tab[] = chaine.split("-");
		int idCategorie = Integer.parseInt(tab[0]);
		String nom = this.txtNom.getText();
		String description = this.txtDescription.getText();
		
		if(choix == 1) { //Si on veut enregistrer (insérer)
			
			//Instancier la classe Sous categorie
			Sous_categorie uneSous_categorie = new Sous_categorie(description, nom, idCategorie);
			
			//Insertion BDD
			Modele.insertSousCategorie(uneSous_categorie);
			
			//Message de réussite
			JOptionPane.showMessageDialog(this, "Insertion réussie");
			
			uneSous_categorie = Modele.selectWhereSous_categorie(idCategorie);
			
			//Actualisation du tableau
			Object ligne[] = {uneSous_categorie.getIdSous_categorie(),uneSous_categorie.getIdCategorie(), uneSous_categorie.getNom(), uneSous_categorie.getDescription()};
			this.unTableau.ajouterLigne(ligne);
			
			//Puis on vide le champs
			this.viderChamps();
		} 
		else { //Si le choix est égal à 2 (modifier)
			int numLigne = uneTable.getSelectedRow();
			int idSous_categorie = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
			//idCategorie = Integer.parseInt(unTableau.getValueAt(numLigne, 1).toString());
			//Instancier la classe Pilote
			Sous_categorie uneSous_categorie = new Sous_categorie(idSous_categorie, description, nom, idCategorie);
			//Mise à jour dans la BDD
			Modele.UpdateSousCategorie(uneSous_categorie);
			//Mise à jour de l'affichage
			Object ligne[] = {uneSous_categorie.getIdSous_categorie(),uneSous_categorie.getIdCategorie(), uneSous_categorie.getNom(), uneSous_categorie.getDescription()};
			unTableau.modifierLigne(numLigne, ligne);
			JOptionPane.showMessageDialog(this, "Modification réusssie");
		}
		this.btEnregistrer.setText("Enregistrer");
		//Vider le formulaire
		this.viderChamps();
	}

}

