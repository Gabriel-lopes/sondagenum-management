package vue;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controleur.Categorie;
import controleur.Sondage;
import controleur.Sondeur;
import controleur.Sous_categorie;
import controleur.Tableau;

import modele.Modele;

public class PanelSondage extends PanelDeBase implements ActionListener 
{
     
	private JPanel panelForm = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btEnregistrer = new JButton("Enregistrer");
	
	private JTextField txtLibelle = new JTextField();
	private JTextField txtDateCreation = new JTextField();
	private JTextField txtEtat = new JTextField();
	private JTextField txtDateFin = new JTextField();
	private JComboBox<String> txtIdEtat = new JComboBox<String>();
	private JComboBox<String> txtIdSondeur = new JComboBox<String>();
	private JComboBox<String> txtIdSousCategorie = new JComboBox<String>();

	
	private JTable uneTable;
	private JScrollPane uneScroll;
	private Tableau unTableau;
	
	private JTextField txtMot = new JTextField();
	private JButton btRechercher = new JButton("Rechercher");
	private JPanel panelRechercher = new JPanel();
	
	
	
	public PanelSondage()
	{
		super(new Color(0x0F417A));
		//Construction du panel Form 
		this.panelForm.setBounds(10, 40, 250, 240);
		this.panelForm.setLayout(new GridLayout(7, 2));
		this.panelForm.add(new JLabel("Libelle : "));
		this.panelForm.add(this.txtLibelle);
		this.panelForm.add(new JLabel("Date creation : "));
		this.panelForm.add(this.txtDateCreation);
		this.panelForm.add(new JLabel("Etat : "));
		this.panelForm.add(this.txtIdEtat);
		this.panelForm.add(new JLabel("Date fin : "));
		this.panelForm.add(this.txtDateFin);
		this.panelForm.add(new JLabel("Id Sondeur : "));
		this.panelForm.add(this.txtIdSondeur);
		this.panelForm.add(new JLabel("Id sous-categorie : "));
		this.panelForm.add(this.txtIdSousCategorie);
		this.panelForm.add(this.btAnnuler);
		this.panelForm.add(this.btEnregistrer);
		this.add(this.panelForm);
		this.panelForm.setVisible(true);
		
		
			
		
		//Construction du panel Rechercher
		this.panelRechercher.setBounds(290, 40, 400, 30);
		this.panelRechercher.setLayout(new GridLayout(1, 3));
		this.panelRechercher.add(new JLabel("Filtrer les sondages par : "));
		this.panelRechercher.add(this.txtMot);
		this.panelRechercher.add(this.btRechercher);
		this.add(this.panelRechercher);
		
		
		//Construction de la JScroll afin de lister les pilotes
		String entetes [] = {"idSondage ", "libelle", "dateCreation", "etat", "dateFin","idSondeur","idSous_categorie"};
		this.unTableau = new Tableau(getSondages(""), entetes);
		//Creer la table 
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(290, 100, 400, 180);
		this.add(this.uneScroll);
		
		
		//	Traitement de la suppression 
		this.uneTable.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				int numLigne = uneTable.getSelectedRow();
				if(e.getClickCount() == 2) 
				{
					int idSondage = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
					int retour = JOptionPane.showConfirmDialog(null, "Voulez-Vous supprimer le sondage", "Suppression du sondage", JOptionPane.YES_NO_OPTION);
					if(retour == 0)
					{
						Modele.deleteSondage(idSondage);
						unTableau.supprimerLigne(numLigne);
					}
				}else if (e.getClickCount() == 1)
				{
					txtLibelle.setText(unTableau.getValueAt(numLigne, 1).toString());
					txtDateCreation.setText(unTableau.getValueAt(numLigne, 2).toString());
					txtIdEtat.setToolTipText(unTableau.getValueAt(numLigne, 3).toString());
					txtDateFin.setText(unTableau.getValueAt(numLigne, 4).toString());
					txtIdSondeur.setToolTipText(unTableau.getValueAt(numLigne, 5).toString());
					txtIdSousCategorie.setToolTipText(unTableau.getValueAt(numLigne, 6).toString());
					btEnregistrer.setText("Modifier");
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//Rendre les boutons ecoutable
		this.btAnnuler.addActionListener(this);
		this.btEnregistrer.addActionListener(this);
		this.btRechercher.addActionListener(this);
		
	}
	
	public void remplirCombo ()
	{
		//remplir les Combos
		ArrayList<String> lesEtats = new ArrayList<String>();
		this.txtIdEtat.removeAllItems();
		this.txtIdEtat.addItem("Ouvert");
		this.txtIdEtat.addItem("Ferme");
		
		ArrayList<Sondeur> lesSondeurs = Modele.selectAllSondeurs(""); 
		this.txtIdSondeur.removeAllItems();
		for (Sondeur unSondeur : lesSondeurs)
		{
			this.txtIdSondeur.addItem(unSondeur.getIdSondeur() + "-" + unSondeur.getNom());
		}
		
		ArrayList<Sous_categorie> lesSous_Categories = Modele.selectAllSousCategories(""); 
		this.txtIdSousCategorie.removeAllItems();
		for (Sous_categorie uneSous_categorie : lesSous_Categories)
		{
			this.txtIdSousCategorie.addItem(uneSous_categorie.getIdSous_categorie() + "-" + uneSous_categorie.getNom());
		}
	}
	
	
	public Object [][] getSondages(String mot)
	{
		//Methode qui transforme l'ArrayList des pilotes en une matrice [] []
		ArrayList<Sondage> lesSondages = Modele.selectAllSondage(mot);
		//Je cree ma matrice
		Object matrice [][] = new Object[lesSondages.size()][7];
		int i = 0;
		for(Sondage unSondage : lesSondages)
		{
			matrice[i][0] = unSondage.getIdSondage();
			matrice[i][1] = unSondage.getLibelle();
			matrice[i][2] = unSondage.getDateCreation();
			matrice[i][3] = unSondage.getEtat();
			matrice[i][4] = unSondage.getDateFin();
			matrice[i][5] = unSondage.getIdSondeur();
			matrice[i][6] = unSondage.getIdSous_categorie();
			i++;
		}
		
		return matrice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.btAnnuler)
		{
			this.viderChamps();
			this.btEnregistrer.setText("Enregistrer");
		}
		else if (e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Enregistrer"))
		{
			this.traitement(1);
		}
		else if (e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Modifier"))
		{
			this.traitement(2);
		}
		else if(e.getSource() == this.btRechercher)
		{
			String mot = this.txtMot.getText();
			this.unTableau.setDonnees(this.getSondages(mot));
		}
	}
	
	public void viderChamps()
	{
		this.txtLibelle.setText("");
		this.txtDateCreation.setText("");
		//this.txtEtat.setText("");
		this.txtDateFin.setText("");
	}
	public void traitement(int choix)
	{
		
		//On récupere les attributs
		String libelle = this.txtLibelle.getText();
		String dateCreation = this.txtDateCreation.getText();
		//String etat = this.txtIdEtat.getText();
		String dateFin = this.txtDateFin.getText();
		
		//Recupération des id pilote et avion
		String chaine = this.txtIdSondeur.getSelectedItem().toString();
		String tab[] = chaine.split("-");
		int idSondeur = Integer.parseInt(tab[0]);
		
		chaine = this.txtIdEtat.getSelectedItem().toString();
		String etat = chaine;
		
		chaine = this.txtIdSousCategorie.getSelectedItem().toString();
		tab = chaine.split("-");
		int idSousCategorie = Integer.parseInt(tab[0]);
		
		if(choix == 1)
		{
			//Instancier la classe vol
			Sondage unSondage = new Sondage(libelle, dateCreation, etat, dateFin, idSondeur, idSousCategorie);
			
			//Insertion du pilote dans la BDD 
			
			Modele.insertSondage(unSondage);
			
			JOptionPane.showMessageDialog(this, "Insertion réussie");
			
			unSondage = Modele.selectWhereSondage(libelle);
			
			//Actualistation de l'affichage
			Object ligne[] = {unSondage.getIdSondage(), unSondage.getLibelle(), unSondage.getDateCreation(), unSondage.getEtat(), unSondage.getDateFin(), unSondage.getIdSondeur(), unSondage.getIdSous_categorie()};
			this.unTableau.ajouterLigne(ligne);
			
		}
		else
		{
			int numLigne = uneTable.getSelectedRow();
			int idSondage = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
			
			//Instancier la classe Pilote
			Sondage unSondage = new Sondage(idSondage, libelle, dateCreation, etat, dateFin, idSondeur, idSousCategorie);		
			//Mise à jour dans la base
			Modele.updateSondage(unSondage);
			
			//Mise à jour de l'affichage
			
			Object ligne[] = {unSondage.getIdSondage(), unSondage.getLibelle(), unSondage.getDateCreation(), unSondage.getEtat(), unSondage.getDateFin(), unSondage.getIdSondeur(), unSondage.getIdSous_categorie()};
			
			unTableau.modifierLigne(numLigne, ligne);
			JOptionPane.showMessageDialog(this, "Modification réussie");
		}
		this.btEnregistrer.setText("Enregistrer");
		//Vider les champs après insertion
		this.viderChamps();
	}
}