package vue;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import controleur.Categorie;
import controleur.Sous_categorie;
import controleur.Tableau;

import modele.Modele;

 
public class Panel_Sous_Categorie extends PanelDeBase implements ActionListener 
{
	private JPanel panelForm = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btEnregistrer = new JButton("Enregistrer");
	
	private JTextField txtNom = new JTextField();
	private JTextField txtDescription = new JTextField();
	
	private JComboBox<String> txtIdCategorie = new JComboBox<String>();
	
	private JTable uneTable;
	private JScrollPane uneScroll;
	private Tableau unTableau;
	
	private JTextField txtMot = new JTextField();
	private JButton btRechercher = new JButton("Rechercher");
	private JPanel panelRechercher = new JPanel();
	
	
	
	public Panel_Sous_Categorie()
	{
		super(new Color(0x0F417A));
		//Construction du panel Form 
		this.panelForm.setBounds(10, 40, 250, 240);
		this.panelForm.setLayout(new GridLayout(4, 2));
		this.panelForm.add(new JLabel("Nom de la sous categorie : "));
		this.panelForm.add(this.txtNom);
		this.panelForm.add(new JLabel("Description de la sous categorie : "));
		this.panelForm.add(this.txtDescription);
		this.panelForm.add(new JLabel("Id categorie: "));
		this.panelForm.add(this.txtIdCategorie);
		this.panelForm.add(this.btAnnuler);
		this.panelForm.add(this.btEnregistrer);
		this.add(this.panelForm);
		this.panelForm.setVisible(true);
		
		
			
		
		//Construction du panel Rechercher
		this.panelRechercher.setBounds(290, 40, 400, 30);
		this.panelRechercher.setLayout(new GridLayout(1, 3));
		this.panelRechercher.add(new JLabel("Filtrer les sous catégories par : "));
		this.panelRechercher.add(this.txtMot);
		this.panelRechercher.add(this.btRechercher);
		this.add(this.panelRechercher);
		
		
		//Construction de la JScroll afin de lister les pilotes
		String entetes [] = {"idSous_categorie ", "nom", "description ", "idCategorie"};
		this.unTableau = new Tableau(getSousCats(""), entetes);
		//Creer la table 
		this.uneTable = new JTable(this.unTableau);
		this.uneScroll = new JScrollPane(this.uneTable);
		this.uneScroll.setBounds(290, 100, 400, 180);
		this.add(this.uneScroll);
		
		
		//	Traitement de la suppression 
		this.uneTable.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				int numLigne = uneTable.getSelectedRow();
				if(e.getClickCount() == 2) 
				{
					int idSousCategorie = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
					int retour = JOptionPane.showConfirmDialog(null, "Voulez-Vous supprimer la sous categorie? ", "Suppression de la sous categorie", JOptionPane.YES_NO_OPTION);
					if(retour == 0)
					{
						Modele.deleteSous_categorie(idSousCategorie);
						unTableau.supprimerLigne(numLigne);
					}
				}else if (e.getClickCount() == 1)
				{
					txtNom.setText(unTableau.getValueAt(numLigne, 1).toString());
					txtDescription.setText(unTableau.getValueAt(numLigne, 2).toString());
					txtIdCategorie.setToolTipText(unTableau.getValueAt(numLigne, 3).toString());
					btEnregistrer.setText("Modifier");
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//Rendre les boutons ecoutable
		this.btAnnuler.addActionListener(this);
		this.btEnregistrer.addActionListener(this);
		this.btRechercher.addActionListener(this);
		
	}
	
	public void remplirCombo ()
	{
		//remplir les Combos 
		ArrayList<Categorie> lesCategories = Modele.selectAllCategories(""); 
		this.txtIdCategorie.removeAllItems();
		for (Categorie unCategorie : lesCategories)
		{
			this.txtIdCategorie.addItem(unCategorie.getIdCategorie()+"-"+ unCategorie.getNom());
		}
	}
	
	
	public Object [][] getSousCats(String mot)
	{
		//Methode qui transforme l'ArrayList des pilotes en une matrice [] []
		ArrayList<Sous_categorie> lesSous_categories = Modele.selectAllSousCategories(mot);
		//Je cree ma matrice
		Object matrice [][] = new Object[lesSous_categories.size()][4];
		int i = 0;
		for(Sous_categorie uneSous_categorie : lesSous_categories)
		{
			matrice[i][0] = uneSous_categorie.getIdSous_categorie();
			matrice[i][1] = uneSous_categorie.getNom();
			matrice[i][2] = uneSous_categorie.getDescription();
			matrice[i][3] = uneSous_categorie.getIdCategorie();
			i++;
		}
		
		return matrice;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.btAnnuler)
		{
			this.viderChamps();
			this.btEnregistrer.setText("Enregistrer");
		}
		else if (e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Enregistrer"))
		{
			this.traitement(1);
		}
		else if (e.getSource() == this.btEnregistrer && e.getActionCommand().equals("Modifier"))
		{
			this.traitement(2);
		}
		else if(e.getSource() == this.btRechercher)
		{
			String mot = this.txtMot.getText();
			this.unTableau.setDonnees(this.getSousCats(mot));
		}
	}
	
	public void viderChamps()
	{
		this.txtNom.setText("");
		this.txtDescription.setText("");
	}
	public void traitement(int choix)
	{
		
		//On récupere les attributs
		String nom = this.txtNom.getText();
		String description = this.txtDescription.getText();
		
		//Recupération des id pilote et avion
		String chaine = this.txtIdCategorie.getSelectedItem().toString();
		String tab[] = chaine.split("-");
		int idCategorie = Integer.parseInt(tab[0]);
		
		if(choix == 1)
		{
			//Instancier la classe vol
			Sous_categorie uneSous_categorie = new Sous_categorie(nom, description, idCategorie);
			
			//Insertion du pilote dans la BDD 
			
			Modele.insertSousCategorie(uneSous_categorie);
			
			JOptionPane.showMessageDialog(this, "Insertion réussie");
			
			uneSous_categorie = Modele.selectWhereSous_categorie(description);
			
			//Actualistation de l'affichage
			Object ligne[] = {uneSous_categorie.getIdSous_categorie(), uneSous_categorie.getNom(), uneSous_categorie.getDescription(), uneSous_categorie.getIdCategorie()};
			this.unTableau.ajouterLigne(ligne);
			
		}
		else
		{
			int numLigne = uneTable.getSelectedRow();
			int idSous_Categorie = Integer.parseInt(unTableau.getValueAt(numLigne, 0).toString());
			
			//Instancier la classe Pilote
			Sous_categorie uneSous_categorie = new Sous_categorie(idSous_Categorie, nom, description, idCategorie);			
			//Mise à jour dans la base
			Modele.UpdateSousCategorie(uneSous_categorie);
			
			//Mise à jour de l'affichage
			
			Object ligne[] = {uneSous_categorie.getIdSous_categorie(), uneSous_categorie.getNom(), uneSous_categorie.getDescription(), uneSous_categorie.getIdCategorie()};
			
			unTableau.modifierLigne(numLigne, ligne);
			JOptionPane.showMessageDialog(this, "Modification réussie");
		}
		this.btEnregistrer.setText("Enregistrer");
		//Vider les champs après insertion
		this.viderChamps();
	}
}